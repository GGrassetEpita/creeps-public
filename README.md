# EPITA Creeps project

## Disclaimer

The aim of this readmer is not only to present this project but also to offer the reader a comprehensive approach
of the context and missing files (to avoid copyrights issues, considering it is issued from EPITA's assistants)

## Objective

The aim of this project is to get experience in the java language, more specially
to manipulate threads.

In this project, one have to implement diverse classes, corresponding to units and buildings.
The objective being to make thoses units evolves in a 3d environment with "ressources" in
order to have at most ressources as possible. All the while resisting assaults from "hordes",
directed at our "nexus" (core building).

The computation of all actions is done locally and one need to send post/get requests to a 
server in order to have the actions done. The server hosts several "players" and is the one
choosing whether or not there is a "horde".

Point are attained by players when they convert blocks, by their units/building number
and resources

Another "game mode" includes "Hector", the garbage collector which goes from one's nexus and mark all
reachables blocks from it. Once it's done, all units not on a reachable block get "collected"
(killed) and all unreachables blocks become void.

## Units :

- Probe : Responsible of collecting minerals and biomass, and constructing the buildings
  
  ##### commands :
  
  scan:1  
  convert  
  move  
  noop (do nothing)  
  release (die)  
  mine:<biomass | minerals>  
  unload  
  spawn:<nexus | pylon | photon-cannon>  
  inspect   
  move:<up | down | east | west | north | south>  
  message
  
- Observer : This unit is useful to discover quickly the map because of its scan range

   ##### commands :
   
   noop  
   message  
   release  
   convert
   move:<up | down | east | west | north | south>  
   scan:<1 | 2 | 4>  
   inspect
   
- Dragoon : A combat unit used to defend against the horde and other players. Don't hit ally units
    
    ##### commands :
    
    noop  
    message  
    release  
    move:<up | down | east | west | north | south>  
    inspect  
    fire:dragoon
    
## Buildings :

- Nexus : Used to spawn units, and is the aim of the horde

    ##### commands :
    
    noop  
    message  
    release
    spawn:<probe | observer | dragoon>  
    inspect  
    fetch  (get the lists of messages issued since last call to fetch)  
    credit (give resources to another player)  

- Pylon : Building allowing to teleport units from one pylon to another
    
    ##### commands :
    
    noop  
    message  
    release  
    teleport  
    inspect  

- Photo Cannon : Defensive unit. Fire a 3-blocks-wide blast within a range of 7 blocks.
    Destroy anything inside its blast.

    ##### commands :
    
    noop  
    message  
    release  
    fire:photo-cannon  
    inspect
    
## Overview of given classes :

#### Hexahedron
Simple 3D Cube containing blocks.

#### Point
Element in space with 3 integer coordinates.

#### Credit
ParameterValue Object in order to send Resources.

#### Fire
ParameterUsed for fire actions.

#### Message
ParameterUsed to create messages to be sent to the server.

#### Teleport
ParameterUsed to teleport agents from a Pylon A to a Pylon B.

#### Report
Parent class that will be differentiated in child subclasses to match server infos on actions performed

#### Response
Basic classes to be filled after receiving infos from the server.

#### Block
Class containing all possible types of block, by blockId.

#### Resources
Class representing the resources of a player.