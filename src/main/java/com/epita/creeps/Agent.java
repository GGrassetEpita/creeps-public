package com.epita.creeps;

import com.epita.creeps.given.extra.Cartographer;
import com.epita.creeps.given.vo.Block;
import com.epita.creeps.given.vo.geometry.Point;
import com.epita.creeps.given.vo.parameter.FireParameter;
import com.epita.creeps.given.vo.parameter.MessageParameter;
import com.epita.creeps.given.vo.report.*;
import com.epita.creeps.given.vo.response.SyncCommandResponse;

import java.util.ArrayList;
import java.util.Random;

import static com.epita.creeps.Tools.*;

public class Agent extends Thread {
    private Integer attemptsMax;
    private Integer biomass;
    private Integer mineral;
    private Point location;
    private boolean isDead;
    private boolean isRunning;
    private final AgentType type;
    private final Integer index;
    private final ServerManager serverManager;
    private final String agentId;
    private final String directionDefault;
    private static Integer indexNext = -1;
    private static ArrayList<Point> converted = new ArrayList<>();

    public Agent(String agentId, Integer attemptsMax, AgentType type, Point location, ServerManager serverManager) {
        isDead = false;
        this.agentId = agentId;
        this.attemptsMax = attemptsMax;
        this.serverManager = serverManager;
        this.type = type;
        synchronized (indexNext) {
            indexNext++;
        }
        index = indexNext;
        this.location = location;
        directionDefault = defaultDirection();
        converted.add(serverManager.gameData.getInitResponse().coordinates);
        mineral = 0;
        biomass = 0;
        isDead = false;
        isRunning = false;
    }

    public SyncCommandResponse<Report> move(String direction) {
        switch (direction) {
            case "south":
                location = new Point(location.x, location.y, location.z - 1);
                break;
            case "north":
                location = new Point(location.x, location.y, location.z + 1);
                break;
            case "west":
                location = new Point(location.x - 1, location.y, location.z);
                break;
            case "east":
                location = new Point(location.x + 1, location.y, location.z);
                break;
            case "up":
                location = new Point(location.x, location.y + 1, location.z);
                break;
            case "down":
                location = new Point(location.x, location.y - 1, location.z);
                break;
        }

        return serverManager.getCommandResponse(agentId, "move:" + direction);
    }

    public SyncCommandResponse<Report> convert() {
        return serverManager.getCommandResponse(agentId, "convert");
    }

    public SyncCommandResponse<Report> mine(String mineral) {
        return serverManager.getCommandResponse(agentId, "mine:" + mineral);
    }

    public SyncCommandResponse<Report> spawn(String unit) {
        return serverManager.getCommandResponse(agentId, "spawn:" + unit);
    }


    public SyncCommandResponse<Report> inspect() {
        return serverManager.getCommandResponse(agentId, "inspect");
    }

    public SyncCommandResponse<Report> scan(Integer range) {
        if (range != 1 && range != 2 && range != 4)
            return null;
        return serverManager.getCommandResponse(agentId, "scan:" + range);
    }

    public SyncCommandResponse<Report> release() {
        return serverManager.getCommandResponse(agentId, "release");
    }

    public SyncCommandResponse<Report> noop() {
        return serverManager.getCommandResponse(agentId, "noop");
    }

    public SyncCommandResponse<Report> message(MessageParameter parameter) {
        return serverManager.getCommandResponse(agentId, "message", parameter, "message");
    }

    public SyncCommandResponse<Report> unload() {
        return serverManager.getCommandResponse(agentId, "unload");
    }

    public SyncCommandResponse<Report> fire(Point aim) {
        return serverManager.getCommandResponse(agentId, "fire:dragoon", new FireParameter(aim), "aim");
    }

    @Override
    public void run() {
        System.out.println("run");
        int count = 1;
        isRunning = true;

        if (type == AgentType.PROBE)
            runProbe();
        else if (type == AgentType.OBSERVER)
            runObserver();
        else if (type == AgentType.DRAGOON)
            runDragoon();
    }

    public void runObserver() {
        Random random = new Random();
        Point worldDimension = serverManager.gameData.getInitResponse().setup.worldDimension;

        while (!isDead) {
            Point destination = new Point(random.nextInt(worldDimension.x), random.nextInt(worldDimension.y),
                    random.nextInt(worldDimension.z));
            goTo(destination, type);

        }
    }

    public void runProbe() {
        int count = 1;
        Point max = serverManager.gameData.getInitResponse().setup.worldDimension;
        Random random = new Random();
        while (serverManager.gameData.isRunning()) {
            convertIfNotConverted();

            if (count % 4 == 0) {
                SyncCommandResponse<Report> response = scan(1);

                Report report;
                do {
                    report = serverManager.getReport(response.reportId);
                } while (report == null || report instanceof NoReportReport);

                if (report.errorCode != null && (report.errorCode.equals("noagent") || report.errorCode.equals("dead"))) {
                    isDead = true;
                    return;
                }

                count = 1;
            }

            Block type = lookAround();
            if (type != null)
                startMining(type);
            if (mineral == serverManager.gameData.getInitResponse().setup.maxProbeMineralsLoad ||
                    biomass == serverManager.gameData.getInitResponse().setup.maxProbeBiomassLoad)
                unloadToBase();


            goTo(new Point(random.nextInt(max.x), random.nextInt(max.y), random.nextInt(max.z)), AgentType.PROBE);
            count++;
        }
    }

    public void runDragoon() {

        for (int i = 0; i < 3; i++) {
            waitValidation(move(defaultDirection()).reportId, serverManager);
        }

        while (!isDead)
        {
            Point aim  = lookAroundEnemies();
            if (aim != null) {
               FireReport report = (FireReport) waitValidation(fire(aim).reportId, serverManager);
                System.out.println("Dragoon fired: " + report.status);
            }
        }
    }

    public Point lookAroundEnemies() {

        Cartographer cartographer = Cartographer.INSTANCE;


        Point aim = null;

        for (int x = location.x - 4; x <= location.x + 4; ++x) {
            for (int y = location.y - 4; y <= location.y + 4; ++y) {
                for (int z = location.z - 4; z <= location.z + 4; ++z) {
                    Point point = new Point(x, y, z);
                    Block block = cartographer.request(point);
                    if (block == Block.HORDE_HEAD || block == Block.HORDE_TAIL || block.isPlayer)
                        return point;
                }
            }
        }

        return aim;
    }

    private String defaultDirection() {
        switch (index % 6) {
            case 0:
                return "north";
            case 1:
                return "south";
            case 2:
                return "west";
            case 3:
                return "east";
            case 4:
                return "up";
            case 5:
                return "down";
        }

        return "north";
    }

    public Block lookAround() {
        Cartographer cartographer = Cartographer.INSTANCE;

        Block type = null;
        Point destination = null;

        for (int x = location.x - 4; x <= location.x + 4; ++x) {
            for (int y = location.y - 4; y <= location.y + 4; ++y) {
                for (int z = location.z - 4; z <= location.z + 4; ++z) {
                    Point point = new Point(x, y, z);
                    Block block = cartographer.request(point);
                    if (isBiomass(block) && (type == null || isBetterBiomass(block, point, type, destination, location))) {
                        type = block;
                        destination = point;
                    } else if (isMineral(block) && (type == null || isBetterMineral(block, point, type, destination, location))) {
                        type = block;
                        destination = point;
                    }
                }
            }
        }
        goTo(destination, AgentType.PROBE);
        return type;
    }

    public void goTo(Point destination, AgentType type) {

        int i = 0;
        if (destination == null)
            return;

        while (location.x < destination.x) {
            System.out.println("x <");
            waitValidation(move("east").reportId, serverManager);
            convertIfNotConverted();
            location = new Point(location.x + 1, location.y, location.z);
            i++;
            if (i % 10 == 0) {
                ScanReport report = (ScanReport) waitValidation(scan(4).reportId, serverManager);
                Cartographer.INSTANCE.register(report);
            }
        }
        while (location.x > destination.x) {
            System.out.println("x >");
            waitValidation(move("west").reportId, serverManager);
            convertIfNotConverted();
            location = new Point(location.x - 1, location.y, location.z);
        }
        while (location.y < destination.y) {
            System.out.println("y <");
            waitValidation(move("up").reportId, serverManager);
            convertIfNotConverted();
            location = new Point(location.x, location.y + 1, location.z);
        }
        while (location.y > destination.y) {
            System.out.println("y >");
            waitValidation(move("down").reportId, serverManager);
            convertIfNotConverted();
            location = new Point(location.x, location.y - 1, location.z);
        }
        while (location.z < destination.z) {
            System.out.println("z <");
            waitValidation(move("north").reportId, serverManager);
            convertIfNotConverted();
            location = new Point(location.x, location.y, location.z + 1);
        }
        while (location.z > destination.z) {
            System.out.println("z >");
            waitValidation(move("south").reportId, serverManager);
            convertIfNotConverted();
            location = new Point(location.x, location.y, location.z - 1);
        }
    }

    private void startMining(Block type) {
        System.out.println("Start mining");
        while (true) {
            if (type == Block.BIOMASS_HIGH || type == Block.BIOMASS_LOW || type == Block.BIOMASS_MEDIUM) {
                SyncCommandResponse<Report> response = mine("biomass");
                MineReport report = (MineReport) waitValidation(response.reportId, serverManager);
                biomass = report.payload.biomass;
                if (report.payload.biomass > serverManager.gameData.getInitResponse().setup.maxProbeBiomassLoad - 5 ||
                        report.resourcesLeft.biomass == 0)
                    return;
                System.out.printf("MINED " + report.payload.biomass + " biomass.");
            } else {
                SyncCommandResponse<Report> response = mine("minerals");
                MineReport report = (MineReport) waitValidation(response.reportId, serverManager);
                mineral = report.payload.minerals;
                if (report.payload.minerals > serverManager.gameData.getInitResponse().setup.maxProbeMineralsLoad - 5 ||
                        report.resourcesLeft.minerals == 0)
                    return;
                System.out.printf("MINED " + report.payload.minerals + " minerals.");
            }
        }

    }

    private void convertIfNotConverted() {
        synchronized (converted) {
            if (!converted.contains(location)) {
                converted.add(location);
                System.out.println("converting");
                waitValidation(convert().reportId, serverManager);
            } else
                return;
        }
    }

    private void unloadToBase() {
        System.out.println("unload to base");
        goTo(serverManager.gameData.getInitResponse().coordinates, AgentType.PROBE);
        waitValidation(unload().reportId, serverManager);
    }

    public enum AgentType {
        PROBE,
        OBSERVER,
        DRAGOON
    }

    public boolean isDead() {
        return isDead;
    }

    public boolean isRunning() {
        return isRunning;
    }
}
