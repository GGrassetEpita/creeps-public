package com.epita.creeps;

import com.epita.creeps.given.vo.parameter.CreditParameter;
import com.epita.creeps.given.vo.parameter.FireParameter;
import com.epita.creeps.given.vo.parameter.MessageParameter;
import com.epita.creeps.given.vo.parameter.TeleportParameter;
import com.epita.creeps.given.vo.report.FetchReport;
import com.epita.creeps.given.vo.report.NoReportReport;
import com.epita.creeps.given.vo.report.Report;
import com.epita.creeps.given.vo.report.SpawnReport;
import com.epita.creeps.given.vo.response.SyncCommandResponse;

import java.util.concurrent.locks.ReentrantLock;

import static com.epita.creeps.Structure.StructType.NEXUS;
import static com.epita.creeps.Tools.waitValidation;

public class Structure extends Thread {
    private final ServerManager serverManager;
    private final String structId;
    private Integer attemptsMax;
    private final StructType structType;
    private boolean isDead;
    private boolean isRunning;

    public Structure(String structId, Integer attemptsMax, StructType structType, ServerManager serverManager) {
        this.serverManager = serverManager;
        this.structId = structId;
        this.attemptsMax = attemptsMax;
        this.structType = structType;
        isDead = false;
    }


    public SyncCommandResponse<Report> noop() {
        return serverManager.getCommandResponse(structId, "noop");
    }

    public SyncCommandResponse<Report> message(MessageParameter parameter) {
        return serverManager.getCommandResponse(structId, "message", parameter, "message");
    }

    public SyncCommandResponse<Report> release() {
        return serverManager.getCommandResponse(structId, "release");
    }

    public SyncCommandResponse<Report> spawn(String unit) {
        return serverManager.getCommandResponse(structId, "spawn:" + unit);
    }

    public SyncCommandResponse<Report> inspect() {
        return serverManager.getCommandResponse(structId, "inspect");
    }

    public SyncCommandResponse<Report> fetch() {
        return serverManager.getCommandResponse(structId, "fetch");
    }

    public SyncCommandResponse<Report> credit(CreditParameter parameter) {
        return serverManager.getCommandResponse(structId, "credit", parameter, "credit");
    }

    public SyncCommandResponse<Report> teleport(TeleportParameter parameter) {
        return serverManager.getCommandResponse(structId, "teleport", parameter, "teleport");
    }

    public SyncCommandResponse<Report> fire(FireParameter parameter) {
        return serverManager.getCommandResponse(structId, "fire:photon-cannon", parameter, "fire:photon-cannon");
    }

    @Override
    public void run() {

        int rec = 0;
        if (structType == NEXUS) {
            int i = 0;

            waitValidation(message(new MessageParameter("Good Luck Have Fun")).reportId, serverManager);
            FetchReport fetchReport = (FetchReport) waitValidation(fetch().reportId, serverManager);
            System.out.println("Fetch : " + fetchReport.messages.toString());
            while (i < 2) {
                if (serverManager.gameData.reserveMinerals > serverManager.gameData.getInitResponse().costs.spawnObserver.minerals
                        && serverManager.gameData.reserveBiomass > serverManager.gameData.getInitResponse().costs.spawnObserver.biomass) {
                    System.out.printf("SPAWN observer");
                    SpawnReport report = (SpawnReport) waitValidation(spawn("observer").reportId, serverManager);
                    serverManager.gameData.reserveMinerals -= serverManager.gameData.getInitResponse().costs.spawnObserver.minerals;
                    serverManager.gameData.reserveMinerals -= serverManager.gameData.getInitResponse().costs.spawnObserver.biomass;
                    i++;
                    ReentrantLock lock = new ReentrantLock();
                    lock.lock();
                    serverManager.gameData.newAgents.add(new Agent(
                            report.agentId, 20, Agent.AgentType.OBSERVER
                            , report.agentLocation, serverManager));
                    lock.unlock();
                }
            }

            while (true) {
                if (serverManager.gameData.reserveMinerals > serverManager.gameData.getInitResponse().costs.spawnProbe.minerals
                        && serverManager.gameData.reserveBiomass > serverManager.gameData.getInitResponse().costs.spawnProbe.biomass) {

                    serverManager.gameData.reserveMinerals -= serverManager.gameData.getInitResponse().costs.spawnProbe.minerals;
                    serverManager.gameData.reserveMinerals -= serverManager.gameData.getInitResponse().costs.spawnProbe.biomass;
                    System.out.println("SPAWN probe");
                    SpawnReport report = (SpawnReport) waitValidation(spawn("probe").reportId, serverManager);

                    ReentrantLock lock = new ReentrantLock();
                    lock.lock();
                    serverManager.gameData.newAgents.add(new Agent(
                            report.agentId, 20, Agent.AgentType.PROBE
                            , report.agentLocation, serverManager));
                    lock.unlock();
                }
                if (rec % 5 == 0) {
                    SyncCommandResponse<Report> response = noop();
                    Report report;
                    do {
                        report = serverManager.getReport(response.reportId);
                    } while (report == null || report instanceof NoReportReport);

                    if (report.errorCode != null && (report.errorCode.equals("dead") || report.errorCode.equals("noagent"))) {
                        isDead = true;
                        return;
                    }
                }
            }
        }
    }

    public boolean isDead() {
        return isDead;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public enum StructType {
        NEXUS,
        PYLON,
        PHOTONCANNON
    }
}
