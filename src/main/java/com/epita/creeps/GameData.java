package com.epita.creeps;

import com.epita.creeps.given.vo.response.InitResponse;

import java.util.LinkedList;

public class GameData {
    Integer reserveMinerals;
    Integer reserveBiomass;
    private boolean isRunning;
    LinkedList<Agent> agents;
    LinkedList<Structure> structures;
    LinkedList<Agent> newAgents;
    private final InitResponse initResponse;

    public GameData(InitResponse initResponse) {
        this.isRunning = false;
        this.initResponse = initResponse;
        reserveBiomass = initResponse.resources.biomass;
        reserveMinerals = initResponse.resources.minerals;
        agents = new LinkedList<>();
        newAgents = new LinkedList<>();
        structures = new LinkedList<>();
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public InitResponse getInitResponse() {
        return initResponse;
    }
}
