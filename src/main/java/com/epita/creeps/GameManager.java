package com.epita.creeps;

import com.epita.creeps.given.extra.Cartographer;
import com.epita.creeps.given.vo.report.NoReportReport;
import com.epita.creeps.given.vo.report.Report;
import com.epita.creeps.given.vo.report.ScanReport;
import com.epita.creeps.given.vo.response.*;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GameManager {
    private Integer actionsNbAttemptsMax;
    private ServerManager serverManager;
    private ExecutorService service;
    private Integer loops;
    private ArrayList<Future> futures;

    public GameManager(Integer actionsNbAttemptsMax) {
        this.actionsNbAttemptsMax = actionsNbAttemptsMax;
    }

    public boolean init(String hostname, String port, String username, Integer nbAttemptsMax) {
        InitResponse initResponse = null;

        for (Integer attempts = 0; initResponse == null && attempts < nbAttemptsMax; attempts++) {
            initResponse = serverManager.postInit(hostname, port, username);
        }
        if (initResponse == null)
            return false;

        serverManager = new ServerManager(hostname, port, username, initResponse);
        serverManager.gameData.setRunning(true);
        serverManager.gameData.agents.add(new Agent(initResponse.probeId, actionsNbAttemptsMax, Agent.AgentType.PROBE,
                initResponse.coordinates, serverManager));
        serverManager.gameData.structures.add(new Structure(initResponse.baseId, actionsNbAttemptsMax, Structure.StructType.NEXUS, serverManager));
        SetupResponse setup = initResponse.setup;
        futures = new ArrayList<>();


        Cartographer.initialize(setup.worldDimension);
        loops = 0;

        return true;
    }

    public StatisticsResponse serverStatistics() {
        return serverManager.getStatistics();
    }

    public StatusResponse serverStatus() {
        return serverManager.getStatus();
    }


    public void start(int nbThreads) {

        SyncCommandResponse<Report> response = serverManager.gameData.agents.get(0).scan(1);
        Report report = response.report;

        while (report == null || report instanceof NoReportReport) {
            report = serverManager.getReport(response.reportId);
        }

        Cartographer.INSTANCE.register((ScanReport) report);

        ExecutorService service = Executors.newFixedThreadPool(nbThreads);
        this.service = service;


        service.submit(serverManager.gameData.agents.get(0));
        Future f = service.submit(serverManager.gameData.structures.get(0));

        System.out.println("in all done");
        while (!allDone()) {
            continue;
        }

        System.out.println("shutdown");
        service.shutdown();
    }

    private boolean allDone() {
        loops++;

        if (serverManager.gameData.newAgents.size() != 0) {
            System.out.println("size :" + serverManager.gameData.newAgents.size());
            serverManager.gameData.agents.addAll(serverManager.gameData.newAgents);
            serverManager.gameData.newAgents.clear();
        }

        for (Agent agent : serverManager.gameData.agents) {
            if (!agent.isDead() && !agent.isRunning()) {
                futures.add(service.submit(agent));
            }
        }


        for (Future future : futures) {
            if (!future.isDone())
                return false;
        }

        return true;
    }

}
