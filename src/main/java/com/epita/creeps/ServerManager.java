package com.epita.creeps;

import com.epita.creeps.given.vo.report.Report;
import com.epita.creeps.given.vo.response.InitResponse;
import com.epita.creeps.given.vo.response.StatisticsResponse;
import com.epita.creeps.given.vo.response.StatusResponse;
import com.epita.creeps.given.vo.response.SyncCommandResponse;

public class ServerManager {
    private String hostname;
    private String port;
    private String username;
    GameData gameData;

    public ServerManager(String hostname, String port, String username, InitResponse initResponse) {
        this.hostname = hostname;
        this.port = port;
        this.username = username;
        gameData = new GameData(initResponse);
    }

    public static InitResponse postInit(String hostname, String port, String username) {
        return Tools.getPostResponse(hostname, port, "init/" + username, InitResponse.class);
    }

    public StatusResponse getStatus() {
        return Tools.getGetResponse(hostname, port, "status", StatusResponse.class);
    }

    public StatisticsResponse getStatistics() {
        return Tools.getGetResponse(hostname, port, "statistics", StatisticsResponse.class);
    }

    public <T extends Report> Report getReport(String reportId) {
        return Tools.getReportResponse(hostname, port, "report/" + reportId);
    }

    public SyncCommandResponse<Report> getCommandResponse(String agentId, String opCode, Object parameter, String parameterName) {

        return Tools.getPostResponse(hostname, port, "command/" + username + "/" + agentId + "/"
                + opCode, SyncCommandResponse.class, parameter, parameterName);
    }

    public SyncCommandResponse<Report> getCommandResponse(String agentId, String opCode) {

        return Tools.getPostResponse(hostname, port, "command/" + username + "/" + agentId + "/"
                + opCode, SyncCommandResponse.class);
    }
}
