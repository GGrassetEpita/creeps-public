package com.epita.creeps;

import com.epita.creeps.given.vo.response.StatusResponse;

public class Program {

    public static void main(String[] args) {

        if (args.length != 3) { // FIXME 3 or 5 ?
            System.err.println("Not enough arguments. Expected 3, got" + args.length);
            System.exit(1);
        }

        Integer nbAttemptsMax = 25;
        GameManager gameManager = new GameManager( 10);

        if (!gameManager.init(args[0], args[1], args[2], nbAttemptsMax)) {
            System.err.println("Could not connect to the server after " + nbAttemptsMax + " attempts.");
            System.exit(1);
        }

        System.out.println("Successfully connected to the server.");

        StatusResponse status;
        do {
            status = gameManager.serverStatus();
        } while (!status.running);

        System.out.println("Game Starts !");

        gameManager.start(20);
    }


}
