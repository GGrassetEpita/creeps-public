package com.epita.creeps;

import com.epita.creeps.given.exception.NoReportException;
import com.epita.creeps.given.json.Json;
import com.epita.creeps.given.vo.Block;
import com.epita.creeps.given.vo.geometry.Point;
import com.epita.creeps.given.vo.report.NoReportReport;
import com.epita.creeps.given.vo.report.Report;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Tools {


    public static <T> T getPostResponse(String hostname, String port, String resource, Class<T> tClass
            ,  Object parameter, String parameterName) {

        Unirest.setObjectMapper(unirestObjectMapperLambda());

        T postResponse;

        try {
            postResponse = Json.parse(Unirest.post("http://" + hostname + ":" + port + "/" + resource)
                    .body(Json.serialize(parameter))
                    .asJson().getBody().toString(), tClass);
        } catch (
                UnirestException e) {
            return null;
        }

        if (postResponse == null) {
            return null;
        }

        return postResponse;
    }

    public static <T> T getPostResponse(String hostname, String port, String resource, Class<T> tClass) {

        Unirest.setObjectMapper(unirestObjectMapperLambda());

        T postResponse;

        try {
            postResponse = Json.parse(Unirest.post("http://" + hostname + ":" + port + "/" + resource)
                    .asJson().getBody().toString(), tClass);
        } catch (
                UnirestException e) {
            return null;
        }

        if (postResponse == null) {
            return null;
        }

        return postResponse;
    }

    public static <T> T getGetResponse(String hostname, String port, String resource, Class<T> tClass) {

        Unirest.setObjectMapper(unirestObjectMapperLambda());

        T getResponse = null;

        try {
            getResponse =  Json.parse(Unirest.get("http://" + hostname + ":" + port + "/" + resource)
                    .asJson().getBody().toString(), tClass);
        } catch (UnirestException e) {
            return null;
        }

        return getResponse;
    }

    public static <T extends Report> Report getReportResponse(String hostname, String port, String resource) {

        Unirest.setObjectMapper(unirestObjectMapperLambda());

        T getResponse = null;

        try {
            getResponse = Json.parseReport(Unirest.get("http://" + hostname + ":" + port + "/" + resource)
                    .asJson().getBody().toString());
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        } catch (NoReportException e) {
            return new NoReportReport(e.reportId,"noreport",e.errorCode);
        }

        return getResponse;
    }

    private static ObjectMapper unirestObjectMapperLambda() {
        return new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    public static boolean isBiomass(Block block) {
        return (block == Block.BIOMASS_LOW) || (block == Block.BIOMASS_MEDIUM) || (block == Block.BIOMASS_HIGH);
    }

    public static boolean isMineral(Block block) {
        return (block == Block.MINERALS_LOW) || (block == Block.MINERALS_MEDIUM) || (block == Block.MINERALS_HIGH);
    }

    public static boolean isBetterBiomass(Block block, Point point, Block bestBlock, Point destination, Point location) {
        boolean type;
        boolean distance;

        type = (block == Block.BIOMASS_HIGH && bestBlock == Block.BIOMASS_MEDIUM) ||
                (block == Block.BIOMASS_HIGH && bestBlock == Block.BIOMASS_LOW) ||
                (block == Block.BIOMASS_MEDIUM && bestBlock == Block.BIOMASS_LOW);
        distance = (dist(location, point) < 1.6 * dist(location, destination));

        return type && distance;
    }

    public static boolean isBetterMineral(Block block, Point point, Block bestBlock, Point destination, Point location) {
        boolean type;
        boolean distance;

        type = (block == Block.MINERALS_HIGH && bestBlock == Block.MINERALS_MEDIUM) ||
                (block == Block.MINERALS_HIGH && bestBlock == Block.MINERALS_LOW) ||
                (block == Block.MINERALS_MEDIUM && bestBlock == Block.MINERALS_LOW);
        distance = (dist(location, point) < 1.6 * dist(location, destination));

        return type && distance;
    }

    public static double dist(Point A, Point B) {
        return sqrt(pow(A.x + B.x, 2) + pow(A.y + B.y, 2) + pow(A.z + B.z, 2));
    }

    public static Report waitValidation(String reportId, ServerManager serverManager) {
        Report report;
        do {
            report = serverManager.getReport(reportId);
        } while (report == null || report instanceof NoReportReport);
        return  report;
    }
}
